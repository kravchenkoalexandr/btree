import java.lang.IllegalArgumentException
import org.junit.jupiter.api.Assertions.assertDoesNotThrow
import org.junit.jupiter.api.DynamicTest
import org.junit.jupiter.api.DynamicTest.dynamicTest
import org.junit.jupiter.api.TestFactory
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.assertThrows
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments
import org.junit.jupiter.params.provider.MethodSource
import org.junit.jupiter.params.provider.ValueSource

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class BNodeTest {
    val keys_1 = listOf(4, -5, -44, 0, 10)
    val keys_2 = listOf(4, -5, 0, 10)
    val keys_3 = listOf(10)
    val keys_4 = emptyList<Int>()

    val unsortedEntries_1 = keys_1.map { Entry(it, it.toString()) }
    val unsortedEntries_2 = keys_2.map { Entry(it, it.toString()) }
    val unsortedEntries_3 = keys_3.map { Entry(it, it.toString()) }
    val unsortedEntries_4 = keys_4.map { Entry(it, it.toString()) }

    val sortedEntries_1 = unsortedEntries_1.sortedBy { it.key }
    val sortedEntries_2 = unsortedEntries_2.sortedBy { it.key }
    val sortedEntries_3 = unsortedEntries_3.sortedBy { it.key }
    val sortedEntries_4 = unsortedEntries_4.sortedBy { it.key }

    val bNode_1 = BNode<Int, String>(8).apply {
        sortedEntries_1.forEach { (key, value) -> addEntry(key, value) }
    }
    val bNode_2 = BNode<Int, String>(8).apply {
        unsortedEntries_1.forEach { (key, value) -> addEntry(key, value) }
    }
    val bNode_3 = BNode<Int, String>(7).apply {
        sortedEntries_1.forEach { (key, value) -> addEntry(key, value) }
    }
    val bNode_4 = BNode<Int, String>(8).apply {
        unsortedEntries_4.forEach { (key, value) -> addEntry(key, value) }
    }
    val bNode_5 = BNode<Int, String>(8).apply {
        sortedEntries_4.forEach { (key, value) -> addEntry(key, value) }
    }
    val bNode_6 = BNode<Int, String>(7).apply {
        sortedEntries_4.forEach { (key, value) -> addEntry(key, value) }
    }
    val unsortedBNodes_1 = unsortedEntries_1.map { (key, value) ->
        BNode<Int, String>(8).apply { addEntry(key, value) }
    }
    val sortedBNodes_1 = sortedEntries_1.map { (key, value) ->
        BNode<Int, String>(8).apply { addEntry(key, value) }
    }

    @ParameterizedTest(name = "Initialization, invalid max number of keys (less then 1), expected: exception")
    @ValueSource(ints = [0, -1, -2, -1000, Int.MIN_VALUE])
    fun `Initialization with invalid maxKeys should throw exception`(maxKeys: Int) {
        assertThrows<IllegalArgumentException> { BNode<Int, String>(maxKeys) }
    }

    @ParameterizedTest(name = "Initialization, valid max number of keys (greater then 0), expected: success init")
    @ValueSource(ints = [1, 2, 3, 10, 100])
    fun `Initialization with valid maxKeys should success`(maxKeys: Int) {
        assertDoesNotThrow { BNode<Int, String>(maxKeys) }
    }

    fun provideDataToCheckEntriesInsert() = listOf(
        Arguments.of(unsortedEntries_1, sortedEntries_1, 8),
        Arguments.of(unsortedEntries_2, sortedEntries_2, 8),
        Arguments.of(unsortedEntries_3, sortedEntries_3, 8),
        Arguments.of(unsortedEntries_4, sortedEntries_4, 8)
    )
    @ParameterizedTest(name = "Add entry, BNode must contain specific values")
    @MethodSource(value = ["provideDataToCheckEntriesInsert"])
    fun `BNode should contain added values`(input: List<Entry<Int, String>>, expected: List<Entry<Int, String>>, maxKeys: Int) {
        val bNode = BNode<Int, String>(maxKeys).apply {
            input.forEach { (key, value) -> addEntry(key, value) }
        }

        expected.forEach { (key, value) -> assert(bNode.get(key) == value) }
    }

    fun provideDataToCheckKeysSorting() = listOf(
        Arguments.of(unsortedEntries_1, sortedEntries_1, 8),
        Arguments.of(sortedEntries_1, sortedEntries_1, 8),
        Arguments.of(unsortedEntries_2, sortedEntries_2, 8),
        Arguments.of(sortedEntries_2, sortedEntries_2, 8),
        Arguments.of(unsortedEntries_3, sortedEntries_3, 8),
        Arguments.of(sortedEntries_3, sortedEntries_3, 8),
        Arguments.of(unsortedEntries_4, sortedEntries_4, 8),
        Arguments.of(sortedEntries_4, sortedEntries_4, 8)
    )
    @ParameterizedTest(name = "Keys must be sorted in descending order")
    @MethodSource(value = ["provideDataToCheckKeysSorting"])
    fun `BNode should sort added keys`(input: List<Entry<Int, String>>, expected: List<Entry<Int, String>>, maxKeys: Int) {
        val bNode = BNode<Int, String>(maxKeys).apply {
            input.forEach { (key, value) -> addEntry(key, value) }
        }

        assert(bNode.entries == expected)
    }

    fun provideDataToCheckEntriesLimit() = listOf(
        Arguments.of(unsortedEntries_1, 1),
        Arguments.of(unsortedEntries_1, 2),
        Arguments.of(unsortedEntries_1, 5),
        Arguments.of(unsortedEntries_1, 8)
    )
    @ParameterizedTest(name = "Add entry, BNode must contain a limited amount of entries")
    @MethodSource(value = ["provideDataToCheckEntriesLimit"])
    fun `BNode should check limited amount of entries`(input: List<Entry<Int, String>>, maxKeys: Int) {
        val bNode = BNode<Int, String>(maxKeys).apply {
            input.forEach { (key, value) -> addEntry(key, value) }
        }

        if (bNode.isEntriesOverflow) {
            assert(bNode.entriesCount > bNode.maxKeys)
        }
    }

    fun provideDataToCheckChildrenLimit() = listOf(
        Arguments.of(emptyList<BNode<Int, String>>(), 2),
        Arguments.of(List(1) { bNode_1 }, 2),
        Arguments.of(List(3) { bNode_2 }, 2),
        Arguments.of(List(10) { bNode_3 }, 2)
    )
    @ParameterizedTest(name = "Add child, BNode must contain a limited amount of children")
    @MethodSource(value = ["provideDataToCheckChildrenLimit"])
    fun `BNode should check limited amount of children`(input: List<BNode<Int, String>>, maxKeys: Int) {
        val bNode = BNode<Int, String>(maxKeys).apply {
            input.forEach { addChild(it) }
        }

        if (bNode.isChildrenOverflow) {
            assert(bNode.childrenCount > bNode.maxChildren)
        }
    }

    fun provideDataToCheckChildrenSorting() = listOf(
        Arguments.of(unsortedBNodes_1, keys_1.sorted(), 8),
        Arguments.of(sortedBNodes_1, keys_1.sorted(), 8)
    )
    @ParameterizedTest(name = "Add child, BNode must sort included children by first keys")
    @MethodSource(value = ["provideDataToCheckChildrenSorting"])
    fun `BNode should sort added children`(input: List<BNode<Int, String>>, expected: List<Int>, maxKeys: Int) {
        val bNode = BNode<Int, String>(maxKeys).apply {
            input.forEach { addChild(it) }
        }

        assert(bNode.children.map { it.getEntry(0).key } == expected)
    }

    fun provideDataToCheckEqualBNodes() = listOf(
        Arguments.of(bNode_1, bNode_1),
        Arguments.of(bNode_1, bNode_2),
        Arguments.of(bNode_4, bNode_4),
        Arguments.of(bNode_4, bNode_5)
    )
    @ParameterizedTest
    @MethodSource(value = ["provideDataToCheckEqualBNodes"])
    fun `BNode should be equal to another BNode`(bNodeOne: BNode<Int, String>, bNodeTwo: BNode<Int, String>) {
        assert(bNodeOne.compare(bNodeTwo))
    }

    fun provideDataToCheckUnequalBNodes() = listOf(
        Arguments.of(bNode_1, bNode_3),
        Arguments.of(bNode_2, bNode_3),
        Arguments.of(bNode_4, bNode_6),
        Arguments.of(bNode_5, bNode_6),
        Arguments.of(bNode_1, bNode_4),
        Arguments.of(bNode_5, bNode_2),
        Arguments.of(bNode_1, bNode_6)
    )
    @ParameterizedTest
    @MethodSource(value = ["provideDataToCheckUnequalBNodes"])
    fun `BNode should not be equal to another BNode`(bNodeOne: BNode<Int, String>, bNodeTwo: BNode<Int, String>) {
        assert(!bNodeOne.compare(bNodeTwo))

    }
}
