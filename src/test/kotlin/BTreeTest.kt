import java.lang.IllegalArgumentException
import org.junit.jupiter.api.Assertions.assertDoesNotThrow
import org.junit.jupiter.api.Assertions.assertNotNull
import org.junit.jupiter.api.Assertions.assertNull
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import org.junit.jupiter.api.extension.ExtensionContext
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments
import org.junit.jupiter.params.provider.ArgumentsProvider
import org.junit.jupiter.params.provider.ArgumentsSource
import org.junit.jupiter.params.provider.MethodSource
import org.junit.jupiter.params.provider.ValueSource
import java.util.stream.Stream

class BTreeTest {
    @ParameterizedTest(name = "Initialization, invalid order (less then 1), expected: exception")
    @ValueSource(ints = [0, -1, -2, -1000])
    fun `Initialization with invalid order should throw exception`(order: Int) {
        assertThrows<IllegalArgumentException> { BTree<Int, String>(order) }
    }

    @ParameterizedTest(name = "Initialization, valid order, expected: no exceptions")
    @ValueSource(ints = [1, 2, 3, 10, 100])
    fun `Initialization with valid order should success`(order: Int) {
        assertDoesNotThrow { BTree<Int, String>(order) }
    }

    @Test
    @DisplayName("Initialization, root must be null, expected: no exceptions")
    fun `BTree root should be null after init`() {
        val bTree = BTree<Int, String>(order = 2)

        assertNull(bTree.root)
    }

    @Test
    @DisplayName("Add entry, root must not be null")
    fun `Root should not be null after adding entry`() {
        val bTree = BTree<Int, String>(order = 2)
        bTree.add(1, "1")

        assertNotNull(bTree.root)
    }

    @Test
    @DisplayName("Add entry, root must contain specific value")
    fun `Root should contain the added entry`() {
        val bTree = BTree<Int, String>(order = 2)
        bTree.add(1, "1")

        bTree.root?.let { assert(it.get(1) == "1") }
    }

    @ParameterizedTest
    @MethodSource(value = ["provideDataToCheckEqualTrees"])
    fun `BTree should be equal to another BTree`(treeOne: BTree<Int, String>, treeTwo: BTree<Int, String>) {
        assert(treeOne.compare(treeTwo))
    }

    @ParameterizedTest
    @MethodSource(value = ["provideDataToCheckUnequalTrees"])
    fun `BTree should not be equal to another BTree`(treeOne: BTree<Int, String>, treeTwo: BTree<Int, String>) {
        assert(!treeOne.compare(treeTwo))
    }

    @ParameterizedTest(name = "Add entry, should be equal to test tree")
    @MethodSource(value = ["provideDataToAddEntry"])
    fun `BTree with added entries should be equal to test BTree`(entries: List<Entry<Int, String>>, expected: BTree<Int, String>) {
        val bTree = BTree<Int, String>(expected.order).apply {
            entries.forEach { (key, value) -> add(key, value) }
        }

        assert(bTree.compare(expected))
    }

    internal class ProviderRemoveData: ArgumentsProvider {
        override fun provideArguments(context: ExtensionContext?): Stream<out Arguments> {
            return Stream.of(
                Arguments.of(entriesForTestTrees, entriesForDelete, testTree_3),
                Arguments.of(entriesForTestTrees, entriesForDelete, testTree_4)
            )
        }
    }

    @Nested
    inner class `Check BTree remove method` {
        @ParameterizedTest
        @ArgumentsSource(ProviderRemoveData::class)
        fun `BTree with removed entries should be equal to test BTree`(
            entries: List<Entry<Int, String>>,
            remove: List<Entry<Int, String>>,
            expected: BTree<Int, String>)
        {
            val bTree = BTree<Int, String>(expected.order).apply {
                entries.forEach { (key, value) -> add(key, value) }
            }

            remove.forEach { bTree.remove(it.key) }

            assert(bTree.compare(expected))
        }
    }

    companion object {
        @JvmField
        val tree_1 = BTree<Int, String>(order = 2).apply {
            val root = BNode<Int, String>(maxKeys)
            for (key in 0 until maxKeys) {
                root.addEntry(key, key.toString())
            }
            this.root = root
        }

        @JvmField
        val tree_2 = BTree<Int, String>(order = 2).apply {
            val root = BNode<Int, String>(maxKeys)
            for (key in 0 until maxKeys) {
                root.addEntry(key, key.toString())
            }
            this.root = root
        }

        @JvmField
        val tree_3 = BTree<Int, String>(order = 3).apply {
            val root = BNode<Int, String>(maxKeys)
            for (key in 0 until maxKeys) {
                root.addEntry(key, key.toString())
            }
            this.root = root
        }

        @JvmField
        val tree_4 = BTree<Int, String>(3).apply {
            val root = BNode<Int, String>(maxKeys)
            for (key in 0 until maxKeys) {
                root.addEntry(key, key.toString())
            }
            this.root = root
        }

        @JvmField
        val testTree_1 = BTree<Int, String>(order = 3).apply {
            root = BNode<Int, String>(maxKeys).apply {
                listOf(6, 14, 26, 48).forEach { _entries.add(Entry(it, it.toString())) }

                val node_0 = BNode(maxKeys, this).apply {
                    listOf(1, 2, 3).forEach { _entries.add(Entry(it, it.toString())) }
                }
                val node_1 = BNode(maxKeys, this).apply {
                    listOf(7, 8, 12).forEach { _entries.add(Entry(it, it.toString())) }
                }
                val node_2 = BNode(maxKeys, this).apply {
                    listOf(16, 17, 25).forEach { _entries.add(Entry(it, it.toString())) }
                }
                val node_3 = BNode(maxKeys, this).apply {
                    listOf(28, 29, 45).forEach { _entries.add(Entry(it, it.toString())) }
                }
                val node_4 = BNode(maxKeys, this).apply {
                    listOf(52, 53, 55, 68).forEach { _entries.add(Entry(it, it.toString())) }
                }

                _children.addAll(listOf(node_0, node_1, node_2, node_3, node_4))
            }
        }

        @JvmField
        val testTree_2 = BTree<Int, String>(order = 2).apply {
            root = BNode<Int, String>(maxKeys).apply {
                listOf(16).forEach { _entries.add(Entry(it, it.toString())) }

                val node_0 = BNode(maxKeys, this).apply {
                    listOf(3, 8).forEach { _entries.add(Entry(it, it.toString())) }

                    val node_0_0 = BNode(maxKeys, this).apply {
                        listOf(1, 2).forEach { _entries.add(Entry(it, it.toString())) }
                    }
                    val node_0_1 = BNode(maxKeys, this).apply {
                        listOf(6, 7).forEach { _entries.add(Entry(it, it.toString())) }
                    }
                    val node_0_2 = BNode(maxKeys, this).apply {
                        listOf(12, 14).forEach { _entries.add(Entry(it, it.toString())) }
                    }

                    _children.addAll(listOf(node_0_0, node_0_1, node_0_2))
                }

                val node_1 = BNode(maxKeys, this).apply {
                    listOf(26, 45, 53).forEach { _entries.add(Entry(it, it.toString())) }

                    val node_1_0 = BNode(maxKeys, this).apply {
                        listOf(17, 25).forEach { _entries.add(Entry(it, it.toString())) }
                    }

                    val node_1_1 = BNode(maxKeys, this).apply {
                        listOf(28, 29).forEach { _entries.add(Entry(it, it.toString())) }
                    }

                    val node_1_2 = BNode(maxKeys, this).apply {
                        listOf(48, 52).forEach { _entries.add(Entry(it, it.toString())) }
                    }

                    val node_1_3 = BNode(maxKeys, this).apply {
                        listOf(55, 68).forEach { _entries.add(Entry(it, it.toString())) }
                    }

                    _children.addAll(listOf(node_1_0, node_1_1, node_1_2, node_1_3))
                }

                _children.addAll(listOf(node_0, node_1))
            }
        }

        @JvmField
        val testTree_3 = BTree<Int, String>(order = 3).apply {
            root = BNode<Int, String>(maxKeys).apply {
                listOf(14, 26).forEach { _entries.add(Entry(it, it.toString())) }

                val node_0 = BNode(maxKeys, this).apply {
                    listOf(2, 6, 7, 8, 12).forEach { _entries.add(Entry(it, it.toString())) }
                }

                val node_1 = BNode(maxKeys, this).apply {
                    listOf(16, 17, 25).forEach { _entries.add(Entry(it, it.toString())) }
                }

                val node_2 = BNode(maxKeys, this).apply {
                    listOf(28, 29, 45, 52, 55).forEach { _entries.add(Entry(it, it.toString())) }
                }

                _children.addAll(listOf(node_0, node_1, node_2))
            }
        }

        @JvmField
        val testTree_4 = BTree<Int, String>(order = 2).apply {
            root = BNode<Int, String>(maxKeys).apply {
                listOf(8, 16, 26, 45).forEach { _entries.add(Entry(it, it.toString())) }

                val node_0 = BNode(maxKeys, this).apply {
                    listOf(2, 6, 7).forEach { _entries.add(Entry(it, it.toString())) }
                }

                val node_1 = BNode(maxKeys, this).apply {
                    listOf(12, 14).forEach { _entries.add(Entry(it, it.toString())) }
                }

                val node_2 = BNode(maxKeys, this).apply {
                    listOf(17, 25).forEach { _entries.add(Entry(it, it.toString())) }
                }

                val node_3 = BNode(maxKeys, this).apply {
                    listOf(28, 29).forEach { _entries.add(Entry(it, it.toString())) }
                }

                val node_4 = BNode(maxKeys, this).apply {
                    listOf(52, 55).forEach { _entries.add(Entry(it, it.toString())) }
                }

                _children.addAll(listOf(node_0, node_1, node_2, node_3, node_4))
            }
        }

        @JvmField
        val entriesForTestTrees = listOf(1, 2, 3, 6, 7, 8, 12, 14, 16, 17, 25, 26, 28, 29, 48, 45, 52, 53, 55, 68)
            .map { Entry(it, it.toString()) }

        @JvmField
        val entriesForDelete = listOf(3, 48, 53, 68, 1).map { Entry(it, it.toString()) }

        @JvmStatic
        fun provideDataToAddEntry() = listOf(
            Arguments.of(entriesForTestTrees, testTree_1),
            Arguments.of(entriesForTestTrees, testTree_2)
        )

        @JvmStatic
        fun provideDataToCheckEqualTrees() = listOf(
            Arguments.of(tree_1, tree_1),
            Arguments.of(tree_1, tree_2),
            Arguments.of(tree_2, tree_2),
            Arguments.of(tree_2, tree_1)
        )

        @JvmStatic
        fun provideDataToCheckUnequalTrees() = listOf(
            Arguments.of(tree_1, tree_3),
            Arguments.of(tree_2, tree_3),
            Arguments.of(tree_1, tree_4),
            Arguments.of(tree_2, tree_4)
        )
    }
}
