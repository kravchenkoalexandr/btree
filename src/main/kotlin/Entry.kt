/**
 * Class for representing [key] [value] pair stored in B-Tree nodes.
 *
 * @param key
 * @param value
 */
data class Entry<K, V> constructor(val key: K, var value: V)
