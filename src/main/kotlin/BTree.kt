/**
 * A class that represents a B-Tree node.
 *
 * @param K key type
 * @param V value type
 * @constructor create B-Tree with order [order]
 */
class BTree<K : Comparable<K>, V>(val order: Int) {
    /**
     * @property nodeMinKeys minimum number of entries that can be contained in a tree node (except the [root])
     */
    val nodeMinKeys = order

    /**
     * @property maxKeys maximum number of entries that can be contained in a tree node
     */
    val maxKeys = 2 * nodeMinKeys

    /**
     * @property nodeMinChildren minimum number of children that can be contained in a tree node (except the [root])
     */
    val nodeMinChildren = nodeMinKeys + 1

    /**
     * @property root of the B-Tree
     */
    var root: BNode<K, V>? = null

    init {
        require(order >= 1) { "Btree order must be greater than or equal to 1" }
    }

    /**
     * Add new entry ([key], [value]) to the tree.
     * @return if the entry with key [key] is already in the tree, return the old value of the entry, else return nil
     */
    fun add(key: K, value: V): V? {
        // Если корень не инициализирован (дерево пустое)
        if (root == null) {
            root = BNode<K, V> (maxKeys).apply { addEntry(key, value) }
            return null
        }
        // Если корень инициализирован, то поиск листьев для вставки
        var currentNode = root
        var oldValue: V? = null
        while (currentNode != null) {
            // Если данный узел пустой (т.е. лист), то добавляем в него
            if (currentNode.isChildrenEmpty) {
                // Добавляем новое значение, запоминаем пред.значение (если оно было)
                oldValue = currentNode.addEntry(key, value)
                // Проверяем, переполнен ли лист
                if (currentNode.isEntriesOverflow) {
                    // Если переполнен, то разбиваем
                    split(currentNode)
                }
                // Возвращаем старое значение
                return oldValue
            }
            // Если не пустой, ищем место для вставки

            currentNode = getIntervalNode(currentNode, key)
        }

        return oldValue
    }

    /**
     * Remove an entry with [key] from the tree.
     * @return if the entry with key [key] is already in the tree, return the old value of the entry, else return nil
     */
    fun remove(key: K): V? {
        val node: BNode<K, V> = getNode(key) ?: return null
        return remove(key, node)
    }

    /**
     * Get a node that contain entry with key [key].
     * @return if the tree contain node with entry with key [key], return this node, else return nil
     */
    fun getNode(key: K): BNode<K, V>? {
        var currentNode: BNode<K, V> = root ?: return null

        var isFoundNext = true
        while (isFoundNext) {
            // Проверяем интервал от -INF до firstKey
            val firstKey = currentNode.firstEntry().key
            // Если попали в интервал
            if (key < firstKey) {
                if (currentNode.isChildrenEmpty) {
                    return null
                } else {
                    currentNode = currentNode.firstChild()
                    continue
                }
            }

            // Проверяем интервал от lastKey до INF
            val lastKey = currentNode.lastEntry().key
            if (key > lastKey) {
                if (currentNode.isChildrenEmpty) {
                    return null
                } else {
                    currentNode = currentNode.lastChild()
                    continue
                }
            }

            // Проверяем остальные интервалы
            isFoundNext = false
            for (i in 0 until currentNode.entriesCount) {
                val currentKey = currentNode.getEntry(i).key
                if (currentKey == key) {
                    return currentNode
                }

                val nextIndex = i + 1
                if (nextIndex < currentNode.entriesCount) {
                    val nextKey = currentNode.getEntry(nextIndex).key
                    if (currentKey < key && nextKey > key) {
                        if (nextIndex < currentNode.childrenCount) {
                            isFoundNext = true
                            currentNode = currentNode.getChild(nextIndex)
                            break
                        } else {
                            return null
                        }
                    }
                }
            }
        }

        return null
    }

    /**
     * Compare two trees.
     * @param to tree to comparison
     * @return true if the trees are equal, or false
     */
    fun compare(to: BTree<K, V>): Boolean {
        val sroot = root
        val troot = to.root

        if (order != to.order) {
            return false
        }
        if (sroot == null && troot == null) {
            return true
        }
        if (sroot == null || troot == null) {
            return false
        }

        return sroot.compare(troot)
    }

    /**
     * Helper method to split a node [node] into two nodes.
     * @see add
     */
    private fun split(node: BNode<K, V>) {
        var currentNode = node
        val entriesCount = currentNode.entriesCount
        val childrenCount = currentNode.childrenCount
        val medianIndex = (entriesCount - 1) / 2
        val medianEntry = currentNode.getEntry(medianIndex)

        // Create left node
        val leftNode = splitNode(currentNode,  0 until medianIndex, 0 until medianIndex + 1)
        // Create right node
        val rightNode = splitNode(currentNode, medianIndex + 1 until entriesCount, medianIndex + 1 until childrenCount)

        val parentNode = currentNode.parent
        if (parentNode == null) {
            // Create new root and add entry in it
            val newRoot = BNode<K, V>(maxKeys).apply { addEntry(medianEntry) }
            // New root = parent of old root
            currentNode.parent = newRoot
            // Set new root
            root = newRoot
            currentNode = newRoot
            // Add to new root two children
            currentNode.addChild(leftNode)
            currentNode.addChild(rightNode)
        } else {
            with(parentNode) {
                // Add new entry to parent node
                addEntry(medianEntry)
                // Remove splited child
                removeChild(currentNode)
                // Add new children
                addChild(leftNode)
                addChild(rightNode)

                // Split parent node if needed
                if (isEntriesOverflow) {
                    split(this)
                }
            }
        }
    }

    /**
     * Helper method to split a node [node] into two nodes (left and right).
     * @see split
     */
    private fun splitNode(node: BNode<K, V>, entryRange: IntRange, childrenRange: IntRange): BNode<K, V> {
        return BNode<K, V>(maxKeys).apply {
            entryRange.forEach { index -> addEntry(node.getEntry(index)) }
            if (!node.isLeaf) {
                childrenRange.forEach { index -> addChild(node.getChild(index)) }
            }
        }
    }

    /**
     * Helper method to get the interval in the node [node] in which the key [key] lies.
     * @see add
     * @return the child node of the given node [node] in which the key [key] lies
     */
    private fun getIntervalNode(node: BNode<K, V>, key: K): BNode<K, V>? {
        // Проверяем интервал от -INF до firstKey
        val firstKey = node.firstEntry().key
        if (key <= firstKey) {
            return if (node.isChildrenEmpty) null else node.firstChild()
        }
        // Проверяем интервал от lastKey до INF
        val lastKey = node.lastEntry().key
        if (key > lastKey) {
            return if (node.isChildrenEmpty) null else node.lastChild()
        }
        // Проверяем интервалы от 2-го ключа до предпоследнего
        for (i in 1 until node.entriesCount) {
            val prevKey = node.getEntry(i - 1).key
            val nextKey = node.getEntry(i).key
            if (key > prevKey && key <= nextKey) {
                return if (node.isChildrenEmpty) null else node.getChild(i)
            }
        }

        return null
    }

    /**
     * Helper method to remove entry with key [key] from given node [node].
     * @see remove
     * @return if the entry with key [key] is already in the subtree with root [node],
     *   return the old value of the entry, else return nil
     */
    private fun remove(key: K, node: BNode<K, V>): V? {
        val index = node.indexOf(key)
        val removed: V? = node.removeEntry(key)
        // Если лист
        if (node.isLeaf) {
            if (node.parent != null && node.entriesCount < nodeMinKeys) {
                combined(node)
            } else if (node == root && node.isEntriesEmpty) {
                root = null
            }
            return removed
        }

        // Если не лист
        val lesserNode = node.getChild(index)
        val greatestNode = getGreatestNode(lesserNode)
        val replaceValue = removeGreatestValue(greatestNode) ?: return null
        node.addEntry(replaceValue)
        if (greatestNode.parent != null && greatestNode.entriesCount < nodeMinKeys) {
            combined(greatestNode)
        }
        if (greatestNode.isChildrenOverflow) {
            split(greatestNode)
        }

        return removed
    }

    /**
     * Helper method to combine children keys in node [node] with parent when size is less than [nodeMinKeys].
     * @see [remove] and [moveNode]
     * @param node
     */
    private fun combined(node: BNode<K, V>) {
        val parent: BNode<K, V> = node.parent ?: return

        val index = parent.indexOf(node)

        val rightNeighborIndex = index + 1
        var rightNeighbor: BNode<K, V>? = null
        var rightNeighborChildren = -nodeMinChildren
        if (rightNeighborIndex < parent.childrenCount) {
            rightNeighbor = parent.getChild(rightNeighborIndex)
            rightNeighborChildren = rightNeighbor.entriesCount
        }

        if (rightNeighbor != null && rightNeighborChildren > nodeMinKeys) {
            combineRightNeighbor(node, rightNeighbor)
            return
        }

        val leftNeighborIndex = index - 1
        var leftNeighbor: BNode<K, V>? = null
        var leftNeighborChildren = -nodeMinChildren
        if (leftNeighborIndex >= 0) {
            leftNeighbor = parent.getChild(leftNeighborIndex)
            leftNeighborChildren = leftNeighbor.entriesCount
        }

        if (leftNeighbor != null && leftNeighborChildren > nodeMinKeys) {
            combineLeftNeighbor(node, leftNeighbor)
            return
        }

        if (rightNeighbor != null && !parent.isEntriesEmpty) {
            swallowNeighbor(node, rightNeighbor, getPreviousEntryIndex(parent, rightNeighbor.firstEntry().key))
            return
        }
        if (leftNeighbor != null && !parent.isEntriesEmpty) {
            swallowNeighbor(node, leftNeighbor, getNextEntryIndex(parent, leftNeighbor.lastEntry().key))
            return
        }
    }

    private fun combineRightNeighbor(node: BNode<K, V>, rightNeighbor: BNode<K, V>) {
        val parent: BNode<K, V> = node.parent ?: return

        val removeEntry = rightNeighbor.firstEntry()
        val previousIndex = getPreviousEntryIndex(parent, removeEntry.key)
        val parentEntry = parent.removeEntry(previousIndex)
        val neighborEntry = rightNeighbor.removeFirstEntry()
        node.addEntry(parentEntry)
        parent.addEntry(neighborEntry)
        if (!rightNeighbor.isChildrenEmpty) {
            node.addChild(rightNeighbor.removeFirstChild())
        }
    }

    private fun combineLeftNeighbor(node: BNode<K, V>, leftNeighbor: BNode<K, V>) {
        val parent: BNode<K, V> = node.parent ?: return

        val removeEntry = leftNeighbor.lastEntry()
        val nextIndex = getNextEntryIndex(parent, removeEntry.key)
        val parentEntry = parent.removeEntry(nextIndex)
        val neighborEntry = leftNeighbor.removeLastEntry()
        node.addEntry(parentEntry)
        parent.addEntry(neighborEntry)
        if (!leftNeighbor.isChildrenEmpty) {
            node.addChild(leftNeighbor.removeLastChild())
        }
    }

    private fun swallowNeighbor(node: BNode<K, V>, neighbor: BNode<K, V>, indexToRemove: Int) {
        val parent: BNode<K, V> = node.parent ?: return

        if (!parent.isEntriesEmpty) {
            val parentEntry = parent.removeEntry(indexToRemove)
            moveNode(fromNode = neighbor, toNode = node, initialEntry = parentEntry)
        }
    }

    /**
     * Helper method to move part of entries and children from node [fromNode] to node [toNode]
     *   (with initial entry [initialEntry]).
     * @see combined
     */
    private fun moveNode(fromNode: BNode<K, V>, toNode: BNode<K, V>, initialEntry: Entry<K, V>) {
        val parent: BNode<K, V> = toNode.parent ?: return

        parent.removeChild(fromNode)
        toNode.addEntry(initialEntry)

        fromNode._entries.forEach { entry -> toNode.addEntry(entry) }
        fromNode._children.forEach { child -> toNode.addChild(child) }

        if (parent.parent != null && parent.entriesCount < nodeMinKeys) {
            combined(parent)
        } else if (parent.isEntriesEmpty) {
            toNode.parent = null
            root = toNode
        }

        return
    }

    /**
     * Helper method to get index of previous entry in node [node] for entry with key [key]
     * @return index of previous entry in node [node] for entry with key [key]
     */
    private fun getPreviousEntryIndex(node: BNode<K, V>, key: K): Int {
        val index = node.entries.drop(1).indexOfFirst { it.key >= key }

        return if (index < 0) node.entriesCount - 1 else index
    }

    /**
     * Helper method to get index of next entry in node [node] for entry with key [key]
     * @return index of next entry in node [node] for entry with key [key]
     */
    private fun getNextEntryIndex(node: BNode<K, V>, key: K): Int {
        val index = node.entries.indexOfFirst { it.key >= key }

        return if (index < 0) node.entriesCount - 1 else index
    }

    /**
     * Helper method to get greatest node in subtree with root [node]
     * @return greatest node in subtree with root [node]
     */
    private fun getGreatestNode(node: BNode<K, V>): BNode<K, V> {
        var currentNode = node
        while (!currentNode.isChildrenEmpty) {
            currentNode = currentNode.lastChild()
        }
        return currentNode
    }

    /**
     * Helper method to remove entry with greatest key in subtree with root [node]
     * @return entry with greatest key in subtree with root [node] (if subtree is not leaf), or null
     */
    private fun removeGreatestValue(node: BNode<K, V>): Entry<K, V>? {
        return if (!node.isEntriesEmpty) node.removeLastEntry() else null
    }
}
