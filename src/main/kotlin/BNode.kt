/**
 * A class that represents a B-Tree node.
 *
 * @param K key type
 * @param V value type
 * @constructor create node of B-Tree with maximum number of keys = [maxKeys] and parent = [parent]
 * @param maxKeys maximum number of entries stored in one node
 * @param parent parent of this node
 */
class BNode<K : Comparable<K>, V>(val maxKeys: Int, var parent: BNode<K, V>? = null) {
    /**
     * @property maxChildren maximum number of children in one node of B-Tree
     */
    val maxChildren = maxKeys + 1

    /**
     * The field will be made private after the testing process.
     *
     * @property _entries list of entries contained in this node
     */
    var _entries: MutableList<Entry<K, V>> = mutableListOf()

    /**
     * The field will be made private after the testing process.
     *
     * @property _children list of children contained in this node
     */
    var _children: MutableList<BNode<K, V>> = mutableListOf()

    /**
     * @property nodeInsertComparator comparator for sorting children
     */
    private val nodeInsertComparator = Comparator<BNode<K, V>> { firstNode, secondNode ->
        val firstKey = firstNode.firstEntry().key
        val secondKey = secondNode.firstEntry().key

        return@Comparator firstKey.compareTo(secondKey)
    }

    init {
        require(maxKeys > 0) { "BNode max keys must be greater than 0" }
    }

    /**
     * @return true if the given node is a leaf (does not contain children)
     */
    val isLeaf: Boolean
        get() = isChildrenEmpty

    /**
     * @return readonly list of children
     */
    val children: List<BNode<K, V>>
        get() = _children.toList()

    /**
     * @return number of children
     */
    val childrenCount: Int
        get() = _children.size

    /**
     * @return true if node does not contain children
     */
    val isChildrenEmpty: Boolean
        get() = _children.isEmpty()

    /**
     * @return true if node contain more than [maxChildren] children
     */
    val isChildrenOverflow: Boolean
        get() = _children.size > maxChildren

    /**
     * @return readonly list of entries
     */
    val entries: List<Entry<K, V>>
        get() = _entries.toList()

    /**
     * @return number of entries
     */
    val entriesCount: Int
        get() = _entries.size

    /**
     * @return true if node does not contain entries
     */
    val isEntriesEmpty: Boolean
        get() = _entries.isEmpty()

    /**
     * @return true if node contain more than [maxKeys] entries
     */
    val isEntriesOverflow: Boolean
        get() = _entries.size > maxKeys

    /**
     * Get value by [key]
     * @return value if it was found by the [key], or null
     */
    fun get(key: K): V? {
        return _entries.firstOrNull { it.key == key }?.value
    }

    /**
     * Get index of entry by [key]
     * @return index of entry if it was found by the [key], or -1
     */
    fun indexOf(key: K): Int {
        return _entries.indexOfFirst { it.key == key }
    }

    /**
     * @return first entry
     */
    fun firstEntry(): Entry<K, V> {
        return _entries.first()
    }

    /**
     * @return last entry
     */
    fun lastEntry(): Entry<K, V> {
        return _entries.last()
    }

    /**
     * @return entry with index [at]
     * @throws IndexOutOfBoundsException
     */
    fun getEntry(at: Int): Entry<K, V> {
        return _entries[at]
    }

    /**
     * Add an entry [entry] to the list of node entries.
     * @return if the [entry] is already in the list of node entries,
     *   returns the old value of the entry with key [entry.key], or null
     */
    fun addEntry(entry: Entry<K, V>): V? {
        return addEntry(entry.key, entry.value)
    }

    /**
     * Add an entry ([key], [value]) to the list of node entries.
     * @return if the entry with [key] is already in the list of node entries,
     *   returns the old value of the entry with key [key], or null
     */
    fun addEntry(key: K, value: V): V? {
        val entry = Entry(key, value)
        val index = indexOf(key)
        if (index != -1) {
            val oldValue = _entries[index].value
            _entries[index] = entry
            return oldValue
        }

        _entries.add(entry)
        _entries.sortBy { it.key }
        return null
    }

    /**
     * Remove an entry with index [at].
     * @return removed entry
     * @throws IndexOutOfBoundsException
     */
    fun removeEntry(at: Int): Entry<K, V> {
        return _entries.removeAt(at)
    }

    /**
     * Remove an entry with key [key].
     * @return value of the removed entry, or nil (if there are no entries with key [key])
     */
    fun removeEntry(key: K): V? {
        val index = indexOf(key)
        if (index == -1) { return null }

        return _entries.removeAt(index).value
    }

    /**
     * Remove first entry.
     * @return removed entry
     */
    fun removeFirstEntry(): Entry<K, V> {
        return removeEntry(0)
    }

    /**
     * Remove last entry.
     * @return removed entry
     */
    fun removeLastEntry(): Entry<K, V> {
        return removeEntry(entriesCount - 1)
    }

    /**
     * @return index of child [child]
     */
    fun indexOf(child: BNode<K, V>): Int {
        return _children.indexOf(child)
    }

    /**
     * @return first child
     */
    fun firstChild(): BNode<K, V> {
        return _children.first()
    }

    /**
     * @return last child
     */
    fun lastChild(): BNode<K, V> {
        return _children.last()
    }

    /**
     * @return child with index [at]
     * @throws IndexOutOfBoundsException
     */
    fun getChild(at: Int): BNode<K, V> {
        return _children[at]
    }

    /**
     * Add child [child] to list of children.
     */
    fun addChild(child: BNode<K, V>) {
        child.parent = this
        _children.add(child)
        _children.sortWith(nodeInsertComparator)
    }

    /**
     * Remove child with index [at].
     * @return removed child
     * @throws IndexOutOfBoundsException
     */
    fun removeChild(at: Int): BNode<K, V> {
        return _children.removeAt(at)
    }

    /**
     * Remove child [child].
     * @return true if the child was removed, or false
     */
    fun removeChild(child: BNode<K, V>): Boolean {
        return _children.remove(child)
    }

    /**
     * Remove first child.
     * @return removed child
     */
    fun removeFirstChild(): BNode<K, V> {
        return removeChild(0)
    }

    /**
     * Remove last child.
     * @return removed child
     */
    fun removeLastChild(): BNode<K, V> {
        return removeChild(childrenCount - 1)
    }

    /**
     * Compare two nodes.
     * @param to node to comparison
     * @return true if the nodes are equal, or false
     */
    fun compare(to: BNode<K, V>): Boolean {
        return maxChildren == to.maxChildren && maxKeys == to.maxKeys &&
            entriesCount == to.entriesCount && childrenCount == to.childrenCount &&
            _entries.zip(to._entries).all { (first, second) -> first == second } &&
            _children.zip(to._children).all { (first, second) -> first.compare(second) }
    }

    override fun toString(): String {
        return "keys=[${_entries.map { it.key }.joinToString()}], keySize=$entriesCount, children=$childrenCount"
    }
}
